package common_method_package;

import request_repository.Endpoints;
import static io.restassured.RestAssured.given;
public class Trigger_Get_API extends Endpoints {
	
	public static int statuscode(String URL) {
		int statuscode = given().header("Content-Type" ,"application/json")
				         .when().get(URL)
				         .then().extract().statusCode();
		
		return statuscode ;
	}
   
	public static String responseBody (String URL) {
	   String responseBody = given().header("Content-Type" ,"application/json")
		                    .when().get(URL)
		                    .then().extract().response().asString();
		                    
		                    return responseBody ;
	}
}
