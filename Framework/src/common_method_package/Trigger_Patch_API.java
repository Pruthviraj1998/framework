package common_method_package;

import static io.restassured.RestAssured.given;

//import request_repository.Endpoints;
import request_repository.Patch_Repository;

public class Trigger_Patch_API extends Patch_Repository {
	public static int extract_statuscode(String requestBody, String URL) {
		int statuscode = given().header("Content-Type", "application/json").body(requestBody).when().patch(URL).then()
				.extract().statusCode();
		return statuscode;

	}

	public static String responseBody(String requestBody, String URL) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().patch(URL)
				.then().extract().response().asString();
		return responseBody;
	}
}
