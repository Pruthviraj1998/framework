  
package request_repository;

public class Endpoints {
	static String hostname = "https://reqres.in/";

//      Post API	
	public static String post_endpoint() {
		String URL = hostname + "api/users";
		System.out.println(URL);
		return URL;

	}

	// Get API
	public static String get_endpoint() {

	 	String URL = hostname + "api/users?page=2";
		System.out.println(URL);
		return URL;

	}

	// Put API

	public static String put_endpoint() {
		String URL = hostname + "api/users/2";
		System.out.println(URL);
		return URL;

		// patch api
	}

	public static String patch_endpoint() {
		String URL = hostname + "api/users/2";
		System.out.println(URL);
		return URL;
	}
}