package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Execl_Data_Reader;

public class Post_Request_Repository extends Endpoints {

	public static String post_requestBody() throws IOException {

		ArrayList<String> excelData = Execl_Data_Reader.Read_Excel_Data("API_Data.xlsx", "Post_API", "Post_TC_1");
		System.out.println(excelData);
		String req_name = excelData.get(1);
		String req_job = excelData.get(2);
		
		
		String requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";

		return requestBody;
	}

}
