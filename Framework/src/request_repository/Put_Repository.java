package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Execl_Data_Reader;

public class Put_Repository extends Endpoints {

	public static String put_requestBody() throws IOException {
		ArrayList<String> Data = Execl_Data_Reader.Read_Excel_Data("API_Data.xlsx", "Put_API", "PUT_TC1");
		System.out.println(Data);
		String req_name = Data.get(1);
		String req_job = Data.get(2); 

		String requestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}\r\n"
				+ "";
		
		return requestBody;
		

	}
}