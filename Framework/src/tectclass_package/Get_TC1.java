package tectclass_package;

import common_method_package.Trigger_Get_API;
import common_utility_package.Handle_logs;
import io.restassured.path.json.JsonPath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Get_TC1 extends Trigger_Get_API {
@Test
	public static void Executer() throws IOException {
		
	File dirname=	Handle_logs.Create_log_Directory("Get_TC1");

		for (int i = 0; i < 5; i++) {

			int statuscode = statuscode(get_endpoint());
			System.out.println(statuscode);

			if (statuscode == 200) {
				String responseBody = responseBody(get_endpoint());
				System.out.println(responseBody);
				
				Handle_logs.evidence_creator(dirname, "Get_TC1", get_endpoint(), null, responseBody);

				Validator(responseBody);
				break;
			} else {
				System.out.println("Desired status code not found hence, Retry");
			
			}
		}
	}
	
	public static void Validator(String responseBody) {

		String[] exp_id_array = { "7", "8", "9", "10", "11", "12" };
		String[] exp_email_array = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] exp_firstname = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] exp_lastname = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] exp_avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		JsonPath jsp_res = new JsonPath(responseBody);

		List<Object> res_data = jsp_res.getList("data");

		int count = res_data.size();
		for (int i = 0; i < count; i++) {
			
			String exp_id = exp_id_array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			Assert.assertEquals(res_id, exp_id);
			
			String exp_email =  exp_email_array[i];
			String res_email = jsp_res.getString("data[" + i + "].email");
			Assert.assertEquals(res_email, exp_email);
			
			String exp_first_name = exp_firstname[i];
			String res_firstname = jsp_res.getString("data[" + i + "].first_name");
			Assert.assertEquals(res_firstname, exp_first_name);
			
			String exp_last_name = exp_lastname[i];
			String res_lastname = jsp_res.getString("data[" + i + "].last_name");
			Assert.assertEquals(res_lastname, exp_last_name);

			String avatar =exp_avatar[i];
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");
			Assert.assertEquals( res_avatar,avatar);

		}
	}
}