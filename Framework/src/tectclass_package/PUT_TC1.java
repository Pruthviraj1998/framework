package tectclass_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_API_Method;
import common_method_package.Trigger_put_API;
import common_utility_package.Handle_logs;
import io.restassured.path.json.JsonPath;
import request_repository.Put_Repository;

public class PUT_TC1 extends Trigger_put_API {

	@Test

	public static void Executer() throws IOException {
		File Dirname = Handle_logs.Create_log_Directory("PUT_TC1");
		String reqbody = put_requestBody();
		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_statuscode(reqbody, put_endpoint());
			System.out.println("Status Code : " + Status_Code);

			if (Status_Code == 200) {
				String ResponseBody = responseBody(reqbody, put_endpoint());
				System.out.println("Response Body : " + ResponseBody);
				Handle_logs.evidence_creator(Dirname, "PUT_TC1", put_endpoint(), put_requestBody(), ResponseBody);
				Validator(reqbody, ResponseBody);
				break;

			} else {
				System.out.println("Desired starus code not found hence, retry");
			}
		}
	}

	public static void Validator(String reqbody, String ResponseBody) {

		JsonPath jsp_req = new JsonPath(reqbody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);
		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 11);

		// Validate

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_updatedAt);

	}

}