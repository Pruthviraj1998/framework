package tectclass_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_Patch_API;
import common_utility_package.Handle_logs;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_Repository;

public class Patch_TC1 extends Trigger_Patch_API {
	@Test
	public static void Executer() throws IOException {

		File Dirname = Handle_logs.Create_log_Directory("Patch_TC1");
		String requestbody = Patch_Repository.patch_requestbody();
		for (int i = 0; i < 5; i++) {
		int Status_Code = extract_statuscode( requestbody, patch_endpoint());
		System.out.println("Status Code : " + Status_Code);
			if (Status_Code == 200) {
				String ResponseBody = responseBody( requestbody, patch_endpoint());
				System.out.println("Response Body : " + ResponseBody);
				Handle_logs.evidence_creator(Dirname, "Patch_TC1", patch_endpoint(), patch_requestbody(), ResponseBody);
				Validator( requestbody , ResponseBody);
				break;
				
			} else {
			System.out.println("Desired status code not found hence, Retry");

			}
		}

	}

	public static void Validator( String  requestbody ,String ResponseBody) throws IOException {

		// Create object of JsonPath to parse the Response body.

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");

		String res_job = jsp_res.getString("job");

		String res_updatedAt = jsp_res.getString("updatedAt");

		String generatedDate = res_updatedAt.substring(0, 10);
		System.out.println(generatedDate);

		LocalDateTime CurrentDate = LocalDateTime.now();

		String newDate = CurrentDate.toString();

		String updatedDate = newDate.substring(0, 10);

		System.out.println(updatedDate);

		// Create an object of JsonPath to parse the Request body.

		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");
		System.out.println(req_name);

		String req_job = jsp_req.getString("job");

		System.out.println(req_job);

		// Validation

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(generatedDate, updatedDate);
	}

}