package tectclass_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_API_Method;
import common_utility_package.Handle_logs;
import io.restassured.path.json.JsonPath;
import request_repository.Post_Request_Repository;

public class Post_TC_1 extends Trigger_API_Method {
	@Test
	public static void Executer() throws IOException {
		File Dirname = Handle_logs.Create_log_Directory("Post_TC_1");
		String requestbody = Post_Request_Repository.post_requestBody();

		for (int i = 0; i < 5; i++) {

			String postURL = post_endpoint();
			int Status_Code = extract_Status_Code(requestbody, postURL);
			System.out.println("Status Code : " + Status_Code);
			if (Status_Code == 201) {

				String ResponseBody = responseBody(requestbody, postURL);
				System.out.println("Response Body : " + ResponseBody);
				Handle_logs.evidence_creator(Dirname, "Post_TC_1", post_endpoint(), post_requestBody(), ResponseBody);
				Validator(requestbody, ResponseBody);
				break;

			} else {
				System.out.println("Desired status code not found hence, Retry");

			}
		}

	}

	public static void Validator(String requestbody, String ResponseBody) throws IOException {

		String requestBody = post_requestBody();

		// Create an object of JsonPath to parse the Response body.

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");

		String res_job = jsp_res.getString("job");

		int res_id = jsp_res.getInt("id");

//		System.out.println("Generated ID : " + res_id);

		String res_createdAt = jsp_res.getString("createdAt");

		String generatedDate = res_createdAt.substring(0, 10);
		System.out.println(generatedDate);

		LocalDateTime CurrentDate = LocalDateTime.now();

		String newDate = CurrentDate.toString();

		String updatedDate = newDate.substring(0, 10);

		System.out.println(updatedDate);

		// object of JsonPath to parse the request body.

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");

//		System.out.println(req_name);

		String req_job = jsp_req.getString("job");

//		System.out.println(req_job);

		// Validation

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(generatedDate, updatedDate);
	}
}